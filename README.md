# Article Rating System

## Getting Started

Before anything run these commands:

```sh
python -m venv venv
# python3 -m venv venv
source venv/bin/activate

pip install -r requirements.txt

python manage.py migrate

python manage.py runserver 

```

Copy all the files in example directory into main directory


# Endpoints

```
POST `{base_url}`/api/v1/account/login

{
    "username": "saradorrii@gmail.com",
    "password": "123456"
}

POST `{base_url}`/api/v1/account/register
{
    "email": "saradorrii@gmail.com",
    "password": "123456"
}

GET `{base_url}`/api/v1/articles

POST `{base_url}`/api/v1/articles/rate
{
    "article": "76b920dc-64c2-4133-a893-20f95c0c3da9",
    "rate": 4
}

GET `{base_url}`/api/v1/articles/`{article_id}`

```

# Note

The `unit test` was not written due to time constraints.
For compute rate avarage when is not in the cache, we could use a ‍‍‍‍‍`cron job`.
