import uuid

from django.db import models
from django.db.models import Avg, Count
from django.core.validators import MinValueValidator, MaxValueValidator

from apps.article.api.services import Rating


class Article(models.Model):
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4,
    )
    title = models.TextField()
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def rate(self):
        cache_rate = Rating.rate(self.id)
        if not cache_rate:
            return self.compute_rate()
        return cache_rate
    
    @property
    def rate_count(self):
        cache_count = Rating.rate_count(self.id)
        if not cache_count:
            return self.compute_rate_count()
        return cache_count
    
    def compute_rate(self):
        return self.rates.aggregate(avg=Avg('rate'))["avg"]

    def compute_rate_count(self):
        return self.rates.aggregate(cnt=Count('rate'))["cnt"]
    
    def user_rate(self, user):
        user_rate = self.rates.filter(user=user)
        if user_rate:
            return user_rate.get().rate
        return None


class ArticleRate(models.Model):
    id = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid.uuid4,
    )
    user = models.ForeignKey(
        "account.User",
        on_delete=models.CASCADE,
        related_name="rates",
    )
    article = models.ForeignKey(
        Article,
        on_delete=models.CASCADE,
        related_name="rates",
    )
    rate = models.IntegerField(
        default=0,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(5),
        ],
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)