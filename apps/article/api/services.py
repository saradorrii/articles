from math import ceil
from django.core.cache import cache


class Rating:

    @classmethod
    def rate(cls, article):
        value = cache.get(article)
        if value:
            return value["rate"]
        return None  # must fetch from articlerate db
    
    @classmethod
    def rate_count(cls, article):
        value = cache.get(article)
        if value:
            return value["count"]
        return None  # must fetch from articlerate db 
    
    def update(article, rate, old_rate=None):
        value = cache.get(article)
        if not value:
            cache.set(article, {"rate": rate, "count": 1})
        else:
            if old_rate:
                new_rate = round(value["rate"] - old_rate + rate / value["count"], 1)
            else:
                new_rate = round(value["rate"] + rate / value["count"] + 1, 1)
                value["count"] += 1
                value["rate"] = new_rate
                cache.set(article, value)


