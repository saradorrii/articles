from django.urls import path

from .views import ArticleListAPIView, ArticleRateCreateAPIView, ArticleAPIView

urlpatterns = [
    path("", ArticleListAPIView.as_view()),
    path("rate", ArticleRateCreateAPIView.as_view()),
    path("<uuid:article_id>", ArticleAPIView.as_view()),
]