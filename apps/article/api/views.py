from rest_framework.pagination import LimitOffsetPagination
from rest_framework.generics import (
    ListAPIView, 
    CreateAPIView, 
    RetrieveAPIView,
)
from rest_framework.permissions import IsAuthenticated

from apps.article.models import Article, ArticleRate
from apps.article.api.serializers import (
    ArticleSerializer,
    ArticleRateCreateSerializer,
    ArticleSerializer,
)

class ArticleListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    search_fields = ["title"]
    ordering = ["created_at"]
    pagination_class = LimitOffsetPagination


class ArticleAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated, )
    queryset = ArticleRate.objects.all()
    serializer_class = ArticleSerializer

    def get_object(self, *args, **kwargs):
        return self.queryset.get(
            user_id=self.request.user.id,
            article_id=self.kwargs["article_id"]
        )


class ArticleRateCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = ArticleRateCreateSerializer
    queryset = ArticleRate.objects.all()
