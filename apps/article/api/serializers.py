from rest_framework import serializers
from apps.article.api.services import Rating

from apps.article.models import Article, ArticleRate
from apps.account.models import User


class CurrentUserIdDefault:
    requires_context = True

    def __call__(self, serializer_field):
        return serializer_field.context["request"].user.pk

    def __repr__(self):
        return "%s()" % self.__class__.__name__


class ArticleSerializer(serializers.ModelSerializer):
    # user_rate = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = (
            "id",
            "title",
            "rate",
            "rate_count",
            # "user_rate",
        )


class ArticleSerializer(serializers.ModelSerializer):
    article = ArticleSerializer()

    class Meta:
        model = ArticleRate
        fields = (
            "rate",
            "article",
        )


class ArticleRateCreateSerializer(serializers.ModelSerializer):
    user_id = serializers.HiddenField(default=CurrentUserIdDefault())

    def create(self, validated_data):
        rate = validated_data.pop("rate")
        article = validated_data.pop("article")
        user_id = validated_data.pop("user_id")

        old_obj, created = ArticleRate.objects.get_or_create(
            defaults={'rate': rate},
            user_id=user_id,
            article=article,
        )
        if not created:
            ArticleRate.objects.filter(id=old_obj.id).update(rate=rate)

        Rating.update(article.id, rate, old_obj.rate)
        return old_obj

    class Meta:
        model = ArticleRate
        fields = (
            "user_id",
            "article",
            "rate",
        )
