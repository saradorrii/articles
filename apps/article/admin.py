from django.contrib import admin

from apps.article.models import Article, ArticleRate


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "title",
        "description",
        "created_at",
    )


@admin.register(ArticleRate)
class ArticleRateAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "user",
        "article",
        "rate",
        "created_at",
    )
