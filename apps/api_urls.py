from django.urls import path, include

urlpatterns = [
    path("account/", include("apps.account.api.urls")),
    path("articles/", include("apps.article.api.urls")),
]