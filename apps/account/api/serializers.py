from django.contrib.auth import get_user_model
from django.contrib.auth.models import update_last_login
from django.utils.translation import gettext_lazy as _

from rest_framework import serializers

from rest_framework_simplejwt.serializers import TokenObtainSerializer
from rest_framework_simplejwt.settings import api_settings
from rest_framework_simplejwt.tokens import RefreshToken

User = get_user_model()


class CustomTokenObtainPairSerializer(TokenObtainSerializer):

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate_username(self, username):
        return username.lower()

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)

        data["refresh"] = str(refresh)
        data["access"] = str(refresh.access_token)

        if api_settings.UPDATE_LAST_LOGIN:
            update_last_login(None, self.user)

        return data


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()

    class Meta:
        model = User
        fields = (
            "email",
            "password",
        )

        extra_kwargs = {
            "password": {
                "write_only": True
            },
        }

    def validate_email(self, email):
        email = email.lower()
        if User.objects.filter(
                email__iexact=email,
        ).exists():
            raise serializers.ValidationError(
                _("user with this email address already exists."))
        return email

    def create(self, validated_data):
        email = validated_data["email"]
        password = validated_data["password"]

        user, _ = User.objects.get_or_create(
            email=email,
            username=email,
        )

        user.set_password(password)
        user.save()

        return user
