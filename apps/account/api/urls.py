from django.urls import path

from apps.account.api.views import CustomTokenObtainPairView, RegisterAPIView

urlpatterns = [
    path("login", CustomTokenObtainPairView.as_view()),
    path("register", RegisterAPIView.as_view()),
]