import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

class User(AbstractUser):

    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    email = models.EmailField(
        _("email address"),
        unique=True,
    )
    username = models.CharField(
        _("username"),
        help_text=
        _("Required. 32 characters or fewer. Lowercase letters, digits _ only; must start with a letter."
          ),
        max_length=256,
        unique=True,
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    